FROM cgialus/alus-devel:latest

RUN apt update
RUN apt install -y curl
RUN apt install -y less
RUN apt install -y ssh
RUN apt install -y vim
RUN cd /root/ && curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
RUN cd /root/ && unzip awscliv2.zip
RUN cd /root/ && ./aws/install
RUN cd /root/ && rm -rf awscliv2.zip
